import time
import pandas as pd

from loguru import logger
from datetime import datetime
from threading import Thread
from requests import Session, Response
from settings import Auth, Url, Config


class ReportRequest:
    def __init__(self, auth_data: Auth, url_data: Url):
        self.headers = {
            'Authorization': f'Bearer {auth_data.TOKEN}'
        }
        self.url = url_data.HOST + url_data.REPORTS_URL

        self.session = Session()
        self.session.headers = self.headers

    def get(self, num_report: str) -> Response:
        """
            params: num_report - уникальный идентификатор репорта
            get запрос на url из config
        """
        return self.session.get(self.url + f'/{num_report}')

    def post(self, num_report: str) -> Response:
        data = {'id': str(num_report)}
        return self.session.post(self.url, json=data)


class Report(ReportRequest):
    def __init__(self, conf: Config, num_report: str, file_name: str):
        super().__init__(conf.AUTH, conf.URL_CONF)
        self.num_report = num_report
        self.file_name = file_name
        self.report = Response()

    def create_and_get_report(self):
        """
            Отправляет запрос на создние report и ожидает его получения
        """
        if not self.create_report():
            return
        self.get_report()

    def create_report(self):
        """
            Отправляет запрос на создание report
        """
        report_create = self.post(self.num_report)
        logger.info(f'POST id-{self.num_report} status_code-{report_create.status_code}')
        if report_create.status_code == 409:
            return False
        return True

    def get_report(self):
        """
            Ожидает Получения report и отправляет на сохранение
        """
        self.report = self.__await_create_report()
        logger.info(f'GET id-{self.num_report} status_code-{self.report.status_code} data-{self.report.json()}')
        if self.report.status_code == 404:
            return False
        self.__save_report(self.report.json())

    def __save_report(self, data: dict):
        """
            Записывает report в файл
        """
        with open(self.file_name, 'a') as t:
            t.write(f'{data.get("id")},{data.get("value")}\n')

    def __await_create_report(self) -> Response:
        """
            Ожидает получения отчета report
        """
        now = self.get(self.num_report)
        while now.status_code == 202:
            time.sleep(1)
            now = self.get(self.num_report)
        return now


class ReportGenerate:
    def __init__(self, config: Config, file_name: str):
        self.config = config
        self.file_name = file_name

    def run_generate(self):
        """
            Запускает генерацию и сохранение report
        """
        Thread(target=self.get_old_report).start()
        while True:
            th = Thread(target=Report(
                self.config,
                str(int(datetime.now().timestamp())),
                self.file_name
            ).create_and_get_report)
            th.start()
            time.sleep(60)

    def get_old_report(self):
        last = self.get_last_id()
        n = 1
        while True:
            rep = Report(
                self.config,
                str(last + 60 * n),
                self.file_name
            )
            th = Thread(target=rep.get_report)
            th.start()
            th.join()
            if rep.report.status_code != 200:
                break
            n += 1

    def get_last_id(self) -> int:
        df = pd.read_csv(self.file_name)
        return df['date_time'].max()
