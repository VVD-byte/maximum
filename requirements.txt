PyYAML==6.0
pandas==0.22.0
requests==2.26.0
threaded==4.1.0
loguru==0.5.3