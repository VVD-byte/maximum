FROM python:3.9.9-alpine3.15

RUN apk update && apk add --no-cache g++ wget

ARG USER=maximum

RUN adduser --disabled-password ${USER} && chown -R ${USER}:${USER} /home/${USER} && chmod 755 /home/${USER}
USER ${USER}
WORKDIR /home/${USER}

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install pandas

COPY . /home/${USER}

CMD ["python", "main.py"]
