import yaml
import os

from dataclasses import dataclass


@dataclass
class Url:
    HOST: str
    REPORTS_URL: str


@dataclass
class Auth:
    TOKEN: str


class Config:
    def __init__(self, config):
        self.URL_CONF = Url(**config.get('URL_CONF'))
        self.AUTH = Auth(**config.get('AUTH'))

    @classmethod
    def get_config(cls, file_config: str):
        if not os.path.exists(file_config):
            raise FileNotFoundError(file_config)
        with open(file_config, "r") as stream:
            return cls(yaml.safe_load(stream))
