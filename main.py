import os

from loguru import logger
from argparse import ArgumentParser
from report_generate import ReportGenerate
from settings import Config


def main(file_config: str, file_name: str):
    check_csv_file(file_name)
    config = Config.get_config(file_config)

    logger.add('report.log', level='INFO', rotation='50 MB')

    ReportGenerate(config, file_name).run_generate()


def check_csv_file(file_name: str):
    if not os.path.exists(file_name):
        with open(file_name, 'w') as t:
            t.write('date_time,value\n')


if __name__ == '__main__':
    pars = ArgumentParser()
    pars.add_argument('-f', '--file', dest='config', help='config file path', default='config.yml')
    pars.add_argument('-fc', '--file_csv', dest='report', help='name for new csv with report', default='report.csv')
    args = pars.parse_args()
    main(args.config, args.report)
